# Web Engineering and Design 1
## Calculator Project
As an practice for the study we develop a calculator in HTML and JS.
### Node
If you are using Node.js the server will run on port 3000 listening to 127.0.0.1 only.
Refer to the node.js and express.js reference for further information.
To install depencies and run the project, follow these steps
1. Open the Terminal/CLI and cd into the root project folder
2. Execute `npm install` to install all depending packages
3. Run `node bin/www` (depending on your system it could also be `node bin\www`)