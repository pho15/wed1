/*global $, document, evaluate, setTimeout, addInput, addOperator*/
/*jslint eqeq: true,*/
$(function () {
    "use strict";
    var display = $('#display');
    var numberFields = $('.numberFields').find('.number');
    var operators = $('.operatorFields').find('input');
    var sum = $('.sumField').find('input');
    var newCalculation = false;

    function throwError(error, saveSate) {
        var state = display.val();
        setTimeout(function () {
            if (saveSate) {
                display.val(state);
            } else {
                display.val("0");
            }
        }, 5000);
        display.val(error);
    }
    $(document).keypress(function (event) {
        var numberFilter = /([0-9])/g;
        var operatorFiler = /([\+\-\*\/])/g;
        var key = String.fromCharCode(event.keyCode);
        if (numberFilter.test(key)) {
            addInput(key);
        } else if (operatorFiler.test(key)) {
            addOperator(key);
        } else if (event.keyCode === 13) {
            display.val(evaluate(display.val()));
            newCalculation = true;
        } else if (event.keyCode == 46 || event.keyCode == 8) {
            display.val('0');
        } else {
            throwError('Only numerical inputs or \'+\', \'-\', \'*\', \'/\' are allowed', true);
        }
    });
    function addInput(number) {
        if (display.val() === "0" || newCalculation) {
            display.val(number);
            newCalculation = false;
        } else {
            display.val(display.val() + number);
        }
    }

    function addOperator(operator) {
        if (display.val() === "0") {
            display.val(operator);
        } else {
            display.val(display.val() + " " + operator + " ");
        }
    }
    function splitExpression(string, operator) {
        var indexOfOperator = string.indexOf(operator);
        var expression = {
            leftPart: $.trim(string.substr(0, indexOfOperator)),
            rightPart: $.trim(string.substr(indexOfOperator + 1))
        };
        return expression;
    }

    function calculate(exp1, exp2, operator) {
        switch (operator) {
        case "+":
            return evaluate(exp1) + evaluate(exp2);
        case "-":
            return evaluate(exp1) - evaluate(exp2);
        case "*":
            return evaluate(exp1) * evaluate(exp2);
        case "/":
            return evaluate(exp1) / evaluate(exp2);
        }
    }

    function evaluate(input) {
        var expression;
        var operator;
        if (parseInt(input, 10) == input) {
            return parseInt(input, 10);
        }
        if (input.indexOf("+") !== -1) {
            operator = "+";
        } else if (input.indexOf("-") !== -1 && input.indexOf("-") !== 0) {
            operator = "-";
        } else if (input.indexOf("*") !== -1) {
            operator = "*";
        } else if (input.indexOf("/") !== -1) {
            operator = "/";
        } else {
            throwError("Invalid term to evaluate", false);
            return;
        }
        expression = splitExpression(input, operator);
        return calculate(expression.leftPart, expression.rightPart, operator);
    }

    numberFields.click(function () {
        addInput(this.value);
    });
    operators.click(function () {
        addOperator(this.value);
    });
    sum.on("click", function () {
        display.val(evaluate(display.val()));
        newCalculation = true;
    });
});
